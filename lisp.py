'''Lisp in Python
Adapted from https://norvig.com/lispy.html
'''
import math
import operator as op


program = "(begin (define r 10) (* pi (* r r)))"

Symbol = str
Number = (int, float)
Atom = (Symbol, Number)
List = list
Exp = (Atom, List)
Bool = bool

class Env(dict):
    "An environment: a dict of {'var': val} pairs, with an outer Env."
    def __init__(self, parms=(), args=(), outer=None):
        self.update(zip(parms, args))
        self.outer = outer
    def find(self, var):
        "Find the innermost Env where var appears."
        return self if (var in self) else self.outer.find(var)

class Procedure(object):
    "A user-defined Scheme procedure."
    def __init__(self, parms, body, env):
        self.parms, self.body, self.env = parms, body, env
    def __call__(self, *args):
        return _eval(self.body, Env(self.parms, args, self.env))

def standard_env() -> Env:
    "An execution environment with some Scheme standard procedures."
    env = Env()
    env.update(vars(math)) # sin, cos, sqrt, pi, etc.
    env.update({
        '+':op.add, '-':op.sub, '*':op.mul, '/':op.truediv,
        '>':op.gt, '<':op.lt, '>=':op.ge, '<=':op.le, '=':op.eq,
        'abs':      abs,
        'append':   op.add,
        'apply':    lambda proc, args: proc(*args),
        'begin':    lambda *x: x[-1],
        'car':      lambda x: x[0],
        'cdr':      lambda x: x[1:],
        'cons':     lambda x,y: [x] + y,
        'eq?':      op.is_,
        'expt':     pow,
        'equal?':   op.eq,
        'length':   len,
        'list':     lambda *x: List(x),
        'list?':    lambda x: isinstance(x, List),
        'map':      map,
        'max':      max,
        'min':      min,
        'not':      op.not_,
        'null?':    lambda x: x == [],
        'number?':  lambda x: isinstance(x, Number),
        'print':    print,
        'procedure?':   callable,
        'round':    round,
        'symbol?': lambda x: isinstance(x, Symbol)
    })
    return env

global_env = standard_env()

def tokenize(chars: str) -> list:
    "Convert a string of characters into a list of tokens."
    return chars.replace('(', ' ( ').replace(')', ' ) ').split()

print(tokenize(program))

def parse(program: str) -> Exp:
    "Read a scheme expression from a string."
    return read_from_tokens(tokenize(program))

def read_from_tokens(tokens: list) -> Exp:
    "Read an expression from a sequence of tokens."
    if len(tokens) == 0:
        raise SyntaxError('unexpectect EOF')
    token = tokens.pop(0)
    if token == '(':
        L = []
        while tokens[0] != ')':
            L.append(read_from_tokens(tokens))
        tokens.pop(0) # pop off ')'
        return L
    elif token == ')':
        raise SyntaxError('unexpected )')
    else:
        return atom(token)

def atom(token: str) -> Atom:
    "Numbers become numbers, every other token is a symbol."
    try: return int(token)
    except ValueError:
        try: return float(token)
        except ValueError:
            return Symbol(token)

print(parse(program))


def _eval(x: Exp, env=global_env) -> Exp:
    "Evaluate an expression in an environment."
    if isinstance(x, Symbol):   # variable reference
        return env.find(x)[x]
    elif not isinstance(x, List):   # constant
        return x
    op, *args = x
    if op == 'quote':
        return args[0]
    elif op == 'if':          # conditional
        (test, conseq, alt) = args
        exp = (conseq if _eval(test, env) else alt)
        return _eval(exp, env)
    elif op == 'define':      # definition
        (_, symbol, exp) = x
        env[symbol] = _eval(exp, env)
    elif op == 'set!':
        (symbol, exp) = args
        env.find(symbol)[symbol] = _eval(exp, env)
    elif op == 'lambda':
        (params, body) = args
        return Procedure(params, body, env)
    else:
        proc = _eval(op, env)
        vals = [_eval(arg, env) for arg in args]
        return proc(*vals)

print(_eval(parse(program)))


def schemestr(exp):
    "Convert a Python object back into a Scheme-readable string."
    if isinstance(exp, List):
        return '(' + ' '.join(map(schemestr, exp)) + ')'
    else:
        return str(exp)

def repl(prompt='lisp.py%< '):
    "A prompt-read-eval-print loop."
    while True:
        val = _eval(parse(input(prompt)))
        if val is not None:
            print(schemestr(val))


if __name__=='__main__':
    repl()